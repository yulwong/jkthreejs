import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeView from '../views/HomeView.vue';
import Login from '../views/Login.vue';
import DView from '../views/DView.vue';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/DView',
    name: 'DView',
    component:DView,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
