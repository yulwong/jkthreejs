import * as THREE from 'three';
export const modeldata = [
  {
    name: '电表',
    path: 'biao.glb',
    imgpath: 'biao.png',
    labelinfo: '我是电表',
    positionx: 0,
    positiony: 0.3,
    positionz: 0,
    scalex: 2,
    scaley: 2,
    scalez: 2,
    rotationx: -Math.PI / 2,
    rotationy: 0,
    rotationz: 0,
    num: 0,
  },
  {
    name: '风速传感器',
    path: 'feng.glb',
    imgpath: 'feng.png',
    labelinfo: '我是风速传感器',
    positionx: 0,
    positiony: 0.3,
    positionz: 0,
    scalex: 2,
    scaley: 2,
    scalez: 2,
    rotationx: 0,
    rotationy: 0,
    rotationz: 0,
    num: 0,
  },
  {
    name: '火情传感器',
    path: 'fire.glb',
    imgpath: 'fire.png',
    labelinfo: '我是火情传感器',
    positionx: 0,
    positiony: 0.3,
    positionz: 0,
    scalex: 4,
    scaley: 4,
    scalez: 4,
    rotationx: 0,
    rotationy: 0,
    rotationz: 0,
    num: 0,
  },
  {
    name: '温湿传感器',
    path: 'wen.glb',
    imgpath: 'wen.png',
    labelinfo: '我是温湿传感器',
    positionx: 0,
    positiony: 0.3,
    positionz: 0,
    scalex: 2,
    scaley: 2,
    scalez: 2,
    rotationx: -Math.PI / 2,
    rotationy: 0,
    rotationz: 0,
    num: 0,
  },
];
