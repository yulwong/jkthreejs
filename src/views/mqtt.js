import mqtt from "mqtt";
// import { post } from "@/service/axios";

export class d3mqtt {
    constructor(d) {
        this.d = d;
    }
    getSubscribes() {
        const scribes=[]
        const d= this.d
        // const d=this.getGates()
        // for (let i=0;i<d.length;i++){
        //     scribes.push("/"+d[i].getDeviceId()+"/cmd")
        // }
        scribes.push("/"+d+"/data")
        return scribes
    }
    //下发指令
    sendMessage(message) {
        //{"sid":"dd01","status":0}
        console.log("sendMessage",message)
        const d= this.d
        // if (d!=null){
        this.client.publish("/"+d+"/cmd", message)
        // }
    }
    //接收指令
    handleMessage(topic, message) {
        const data={
            "topic":topic,
            "message":JSON.parse(message)
        }
        console.log("setmqttdata",data)

        window.setmqttdata(data)
    }
    getMqtt() {
        // const d=this.getGate()
        return "ws://111.229.112.157:8083/mqtt"
    }
    
    run() {
        //运行
        // console.log("g6 run");
        const self = this;
        const options = {
            username: "",
            password: "",
            cleanSession: false,
            keepAlive: 60,
            clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
            connectTimeout: 4000
        }
        this.client = mqtt.connect(this.getMqtt(), options);
        this.client.on("connect", (e) => {
            this.client.subscribe(this.getSubscribes(), { qos: 0 }, (err) => {
                // console.log("err===",err)
                if (!err) {
                    console.log("订阅成功",);
                    //监听获取信息
                    this.client.on("message", (topic, message) => {
                        //{"sid": "num05", "type": 0, "data": "a"}
                        // console.log("getMessage",topic,message.toString())
                        self.handleMessage(topic, message.toString())
                    });
                } else {
                    //todo 提示给用户
                    alert('消息订阅失败！')
                }
            },
            );
            console.log("成功连接服务器:", e);
        });
    }
    stop() {
        if (this.client) {
            console.log("断开连接服务器");
            this.client.end()
        }
    }
}